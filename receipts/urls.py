from django.urls import path
from receipts.views import (
    account_list, category_list, create_category, create_receipt, receipt_list, create_account
    )


urlpatterns = [
    path('create/', create_receipt, name='create_receipt'),
    path("", receipt_list, name="home"),
    path('accounts/', account_list, name='account_list'),
    path('categories/', category_list, name='category_list'),
    path('category/create/', create_category, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),
]
