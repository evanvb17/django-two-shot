from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt
# Register your models here.


@admin.register(Account)
class AccountsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
    )


@admin.register(ExpenseCategory)
class ExpensesAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
    )
