from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from .forms import LoginForm
from receipts.models import ExpenseCategory, Account


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                form.add_error(None, 'Invalid username or password')
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            # check if passwords match
            password = form.cleaned_data.get('password1')
            password_confirmation = form.cleaned_data.get('password2')
            if password != password_confirmation:
                form.add_error('password2', 'The passwords do not match')
            else:
                # create new user and log them in
                user = form.save()
                user = authenticate(username=user.username, password=password)
                login(request, user)
                return redirect('project-list')
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})

